
                  What plane is that??
                 ======================

This simple program uses the OpenSky API to list planes nearby, ordering
them by distance from a focal point.

It was written to answer the question: "What plane is that?" which
frequently pops up as a particularly loud plane passes overhead.

Sample output:

Distance  dir Callsign  baro alt   cat     velo    squawk   lat      lon
 5.86 km  SE   CFE39M             large     4 km/h  1375   51.5046    0.0465   United Kingdom
 5.96 km  SE   KLM993                      17 km/h  6270   51.5051    0.0504  KLM Cityhopper Kingdom of the Netherlands
 6.65 km  SE   CFE37E                       3 km/h  None   51.5049    0.0657   United Kingdom
 6.71 km  SE   N500EU                       6 km/h  None   51.5041    0.0657   United States
 7.64 km  NW   UAE41     7550 ft          506 km/h  1104   51.5793   -0.0983  Emirates United Arab Emirates
 8.03 km  SE   BAW38     5750 ft  HEAVY   322 km/h  3233   51.4774    0.0157  British Airways United Kingdom
14.30 km  SW   ETD57X    3600 ft  HEAVY   282 km/h  3127   51.4654    -0.156   United Arab Emirates
19.57 km  SW   BAW63XT  13050 ft          694 km/h  2251   51.3757   -0.0482  British Airways United Kingdom
21.14 km  SE   GBANX     1300 ft  small   204 km/h  None   51.3609    0.0447   United Kingdom


To use the script:

1. Modify the BOUNDING_BOX and FOCAL_POINT parameters at the top of the
   script to the area around you. https://openstreetmap.org can help find
   the relevant numbers.

2. Install the requests package:

   pip install requests

3. Run the script with:

   python3 plane_ident.py

Enhancements, bug fixes and comments welcome!


Copyright (C) 2023 Kevin Steen <code at kevinsteen net>

This is Libre/Free Software - copying, modifying and re-distribution are
permitted under the terms of the GNU Affero General Public License v3 or
later (AGPL3+). See the file COPYING for details.

Latest release: v1.0.20231101 - November 2023
