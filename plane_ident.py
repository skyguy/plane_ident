#!/usr/bin/python3
#
# Uses the OpenSky API to identify planes nearby
#
#  Copyright 2023 Kevin Steen <code at kevinsteen.net>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

FOCAL_POINT = (51.5490, 0.0009)  # latitude, longitude

BOUNDING_BOX = (51.3, 51.7, -0.2, 0.2)  # east London
#BOUNDING_BOX = (42.0, 42.4, -71.3, -71.0)  # boston
# bbox = (min latitude, max latitude, min longitude, max longitude)
#      ie South, North, West, East

AIRLINES = ("""
    AEE Aegean Airlines
    AHY Azerbaijan Airlines
    ALK SriLankan Airlines
    AWC Titan Airways(Gambia?)
    BAW British Airways
    BEL Brussels Airlines
    CPA Cathay Pacific
    DAH Air Algerie
    DLH Lufthansa
    EFW BA Euroflyer
    EZY easyJet
    GFA Gulf Air
    ICE Icelandair Cargo
    KLM KLM Cityhopper
    MAI Mauritania Airlines
    MSR Egyptair
    QTR Qatar Airways
    RYR Ryanair
    SZN Air Senegal
    TAP TAP Air Portugal
    THY Turkish Airlines
    TRA Transavia
    UAE Emirates
    UPS UPS
    VIR Virgin Atlantic
    VTI Vistara
    WUk Wizz Air UK
    """)

# SOCKS5 proxy for the HTTP request:
proxies = {  # socks5h causes DNS resolution on the proxy
    'https': "socks5h://localhost:9150",  # Tor Browser
    'http': "socks5h://localhost:9150",
    #'https': "socks5h://localhost:9050",  # System Tor
    #'http': "socks5h://localhost:9050",
    }
proxies = None
# Comment the line above to use proxies


import opensky
from opensky import StateVector
from math import radians, sin, cos, atan2, sqrt


def get_live_data():
    api = opensky.OpenSkyApi(proxies=proxies)
    states = api.get_states(bbox = BOUNDING_BOX)

    with open("res.txt", "at") as result:
        result.write(str(states) + "\n")

    return states.states


def crow_distance_km(lat1, lon1, lat2, lon2):
    # function for calculating ground distance between two lat-long locations
    R = 6373.0 # approximate radius of earth in km.

    lat1 = radians( float(lat1) )
    lon1 = radians( float(lon1) )
    lat2 = radians( float(lat2) )
    lon2 = radians( float(lon2) )

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = float(format( R * c , '.2f' )) #rounding. From https://stackoverflow.com/a/28142318/4355695
    return distance


def get_test_data():
    def dict_values(item):
        return item
    states = [   StateVector(dict_values(['a63a67', 'N500EU  ', 'United States', 1699378571, 1699378572, 0.0657, 51.5041, None, True, 1.8, 64.69, None, None, None, None, False, 0, 0])),
                      StateVector(dict_values(['89631c', 'UAE41   ', 'United Arab Emirates', 1699378580, 1699378580, -0.0983, 51.5793, 2301.24, False, 140.45, 130.69, -5.2, None, 2293.62, '1104', False, 0, 1])),
                      StateVector(dict_values(['896463', 'ETD57X  ', 'United Arab Emirates', 1699378580, 1699378580, -0.156, 51.4654, 1097.28, False, 78.2, 270.38, -4.88, None, 1112.52, '3127', False, 0, 6])),
                      StateVector(dict_values(['484c54', 'KLM993  ', 'Kingdom of the Netherlands', 1699378564, 1699378565, 0.0504, 51.5051, None, True, 4.63, 185.62, None, None, None, '6270', False, 0, 1])),
                      StateVector(dict_values(['4075a2', 'CFE39M  ', 'United Kingdom', 1699378430, 1699378430, 0.0465, 51.5046, None, True, 1.03, 180, None, None, None, '1375', False, 0, 4])),
                      StateVector(dict_values(['406f78', 'BAW38   ', 'United Kingdom', 1699378580, 1699378580, 0.0157, 51.4774, 1752.6, False, 89.34, 241.07, -4.23, None, 1744.98, '3233', False, 0, 6])),
                      StateVector(dict_values(['407777', 'CFE37E  ', 'United Kingdom', 1699378559, 1699378559, 0.0657, 51.5049, None, True, 0.77, 281.25, None, None, None, None, False, 0, 0])),
                      StateVector(dict_values(['406ae3', 'BAW63XT ', 'United Kingdom', 1699378580, 1699378580, -0.0482, 51.3757, 3977.64, False, 192.87, 93.98, 13.33, None, 3947.16, '2251', False, 0, 1])),
                      StateVector(dict_values(['401a19', 'GBANX   ', 'United Kingdom', 1699378580, 1699378580, 0.0447, 51.3609, 396.24, False, 56.6, 101, -2.93, None, 419.1, None, False, 0, 3]))
                      ]
    return states


def enhance(records):
    VEHICLE_CATEGORIES = {
        0: "",
        1: "",
        2: "light",
        3: "small",
        4: "large",
        5: "BIG",
        6: "HEAVY"
        }

    res = []
    fp_lat = FOCAL_POINT[0]; fp_lon = FOCAL_POINT[1]
    airlines = {}
    lines = AIRLINES.strip().split("\n")
    for line in lines:
        line = line.strip()
        airlines[line[:3].upper()] = line[4:]

    for r in records:
        r.dist = crow_distance_km(fp_lat, fp_lon, r.latitude, r.longitude)
        r.feet = " " * 8
        if r.baro_altitude:
            r.feet = format(float(r.baro_altitude) * 3.2808, "5.0f") + " ft"
        if r.velocity:
            r.velo_kmh = format(float(r.velocity) / 1000 * 3600, "3.0f") + " km/h"
        r.cat = VEHICLE_CATEGORIES.get(r.category, r.category)
        r.airline = airlines.get(r.callsign[:3], "")

        # Direction of vehicle from focal point:
        dir1 = "N"
        if fp_lat > r.latitude: dir1 = "S"
        dir2 = "E"
        if fp_lon > r.longitude: dir2 = "W"
        r.dir = f"{dir1}{dir2}"

        res.append(r)

    return res


def main():
    #states = get_test_data()
    states = get_live_data()

    if states:
        states = enhance(states)
        print("Distance  dir Callsign  baro alt   cat     velo    "
            "squawk   lat      lon")
        for a in sorted(states, key=lambda i: i.dist):
            print(
                f"{a.dist:5.2f} km  {a.dir}   {a.callsign} {a.feet}  {a.cat:^5}  ",
                #f"{a.velocity:^3.2f} m/s  "
                f"{a.velo_kmh}  {a.squawk}  {a.latitude:8} ",
                #f"{FOCAL_POINT} ",
                f"{a.longitude:8}  {a.airline} {a.origin_country}",
                #f"\n{str(a.__dict__)}"
                )
    else:
        print("No results")

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e)
    finally:
        input("\nPress Enter to close\n")

